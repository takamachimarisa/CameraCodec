package org.camera.activity;

import com.example.cameracodec.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;

import org.camera.camera.CameraWrapper;
import org.camera.camera.CameraWrapper.CamOpenOverCallback;
import org.camera.floatingwindow.FloatingWindow;
import org.camera.textureview.*;

@SuppressLint("NewApi")
public class CameraViewActivity extends Activity implements CamOpenOverCallback{
	private static final String TAG = "CameraViewActivity";
	private CameraTextureView mCameraTexturePreview;
	private float mPreviewRate = -1f;
	
	// ��������
	private FloatingWindow mFloatingWindow = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera_preview);
		initUI();
		initViewParams();
	}
	
	@Override  
    protected void onStart() {  
		Log.i(TAG, "onStart");
        super.onStart();  
        
        Thread openThread = new Thread() {
			@Override
			public void run() {
				CameraWrapper.getInstance().doOpenCamera(CameraViewActivity.this);
			}
		};
		openThread.start();
    }
	
	private void initUI() {
		mCameraTexturePreview = (CameraTextureView) findViewById(R.id.camera_textureview);
	}
	
	private void initViewParams() { 
		DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();  
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels; 
        
        ViewGroup.LayoutParams params = mCameraTexturePreview.getLayoutParams();
        params.width = screenWidth;
        params.height = screenHeight;
        this.mPreviewRate = (float)params.height / (float)params.width; 
        mCameraTexturePreview.setLayoutParams(params);
	}

	@Override
	public void cameraHasOpened() {
		SurfaceTexture camerasurface = this.mCameraTexturePreview.getSurfaceTexture();
		CameraWrapper.getInstance().doStartPreview(camerasurface, mPreviewRate);
	}
	
    @Override
    public void onResume(){
    	super.onResume();
    	createFloatingWindow();
    }
    
    @Override
    public void onPause(){
    	super.onPause();
    	destoryFloatingWindow();
    }
	
    private void createFloatingWindow() {   
    	mFloatingWindow = new FloatingWindow(getApplicationContext());
    	if(mFloatingWindow != null) {
    		mFloatingWindow.show();
    	}
    }
    
    private void destoryFloatingWindow() {
    	if(mFloatingWindow != null) {
			mFloatingWindow.close();
	    	mFloatingWindow = null;
    	}
    }
}
