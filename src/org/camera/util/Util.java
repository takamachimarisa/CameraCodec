package org.camera.util;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Util {

	public static int convertPixelToDp(float px) {
		DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / metrics.density;
        return (int)dp;
    }
	
	public static int convertDpToPixel(float dp) {
		DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = metrics.density * dp;
        return (int)px;
    }
}
