package org.camera.textureview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.util.Log;
import android.view.TextureView;
import org.camera.camera.*;

@SuppressLint("NewApi")
public class CameraTextureView extends TextureView implements TextureView.SurfaceTextureListener {
	private final String TAG = "CameraTextureView";
	Context mContext;  
    SurfaceTexture mSurface; 
	public CameraTextureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
	    this.setSurfaceTextureListener(this);
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
		Log.i(TAG, "onSurfaceTextureAvailable()");
		this.mSurface = surface;
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
		Log.i(TAG, "onSurfaceTextureSizeChanged()");  
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		Log.i(TAG, "onSurfaceTextureDestroyed()");  
		CameraWrapper.getInstance().doStopCamera();
		return false;
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
		Log.i(TAG, "onSurfaceTextureUpdated()");
	}

	public SurfaceTexture getSurfaceTexture() {
		return this.mSurface;
	}
}
