package org.camera.config;

public class ConfigEntity {
	public static int RESOLUTION_WIDTH = 1280;
	public static int RESOLUTION_HEIGHT = 720;
	public static int VIDEO_BITRATE = 1000*1000;					// 视频码率
	public static int VIDEO_FPS = 15;								// 视频帧率
	public static int CODEC_IFRAME_INTERVAL = 4;					// codec I帧间隔(秒)
	public static int VIDEO_AUTO_ROTATION = 1;						// 本地视频自动旋转控制（参数为int型， 0表示关闭， 1 开启[默认]，视频旋转时需要参考本地视频设备方向参数）
}
