package org.camera.floatingwindow;

import org.camera.application.CameraApplication;
import org.camera.codec.MediaCodecManager;
import org.camera.util.Util;

import com.example.cameracodec.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.WindowManager.LayoutParams;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

public class FloatingWindow  {
	private static final String TAG = "FloatingWindow";
	private float mTouchStartX;
    private float mTouchStartY;
    private float x;
    private float y;
    
    private Context mContext = null;
    private static FloatingWindow mFloatingWindow = null;
    //WindowManager相关, wmParams为获取的全局变量，用以保存悬浮窗口的属性
    private WindowManager mWindowManager = null;
    private WindowManager.LayoutParams mWindowParams = null;
    // floating icon
	private IconView mIconView = null;
    // floating video
	private ViewGroup mVideoView = null;
	private SurfaceView mSurfaceView = null;
    private int mVideoWidth = 0;
    private int mVideoHeight = 0;
    // icon和video的切换状态
	private boolean mIsShowVideo = false;
	private boolean mIsShowIcon = false;
    // 用于监听复杂输入事件，如双击
    private GestureDetector mGestureDetector = null;

    class IconView extends ImageView {

		public IconView(Context context) {
			super(context);
		}
		
		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			onWindowTouchEvent(event);
			return false;
		}
    	
    };
    
    public static synchronized FloatingWindow getInstance(Context context) {
    	if(mFloatingWindow == null) {
    		mFloatingWindow = new FloatingWindow(context);
    	}
    	return mFloatingWindow;
    }

	public FloatingWindow(Context context) {
		// 保存context
		mContext = context;
        // 构造GestureDetector对象，传入监听器对象
        mGestureDetector = new GestureDetector(context, mOnGestureListener);
        // init icon view
        mIconView = new IconView(mContext);
        mIconView.setImageResource(R.drawable.floating_icon);
        // init floating video
        mVideoView = (ViewGroup)View.inflate(mContext, R.layout.floating_video_view, null);
        mVideoView.setOnTouchListener(mViewOnTouchListener);
        mSurfaceView = (SurfaceView)mVideoView.findViewById(R.id.floating_surfaceview);
        mSurfaceView.getHolder().addCallback(mSurfaceHolderCallback);
        
        // 初始化window
        initWindow();
	}

	public void show() {
		showIcon();
	}
	
	public void close() {
		closeIcon();
		closeVideo();
	}

    private void initWindow() {
		// 获取WindowManager
	    mWindowManager = (WindowManager)mContext.getApplicationContext().getSystemService("window");
	    mWindowParams = ((CameraApplication)mContext.getApplicationContext()).getWindowManagerParams();

         /**
         *以下都是WindowManager.LayoutParams的相关属性
         * 具体用途可参考SDK文档
         */
        mWindowParams.type = LayoutParams.TYPE_PHONE;   //设置window type
        mWindowParams.format = PixelFormat.RGBA_8888;   //设置图片格式，效果为背景透明

        //设置Window flag
        mWindowParams.flags = LayoutParams.FLAG_NOT_TOUCH_MODAL 
        		| LayoutParams.FLAG_NOT_FOCUSABLE;
        /*
         * 下面的flags属性的效果形同“锁定”。
         * 悬浮窗不可触摸，不接受任何事件,同时不影响后面的事件响应。
         mWindowParams.flags = LayoutParams.FLAG_NOT_TOUCH_MODAL 
				| LayoutParams.FLAG_NOT_FOCUSABLE
				| LayoutParams.FLAG_NOT_TOUCHABLE;
        */
        
        mWindowParams.gravity = Gravity.LEFT | Gravity.TOP;   //调整悬浮窗口至左上角
        //以屏幕左上角为原点，设置x、y初始值
        mWindowParams.x = 0;
        mWindowParams.y = 0;
        
        //设置悬浮窗口长宽数据
        mWindowParams.width = Util.convertDpToPixel(64);
        mWindowParams.height = Util.convertDpToPixel(64);
    }
    
	private boolean onWindowTouchEvent(MotionEvent event) {
		// 在onTouchEvent方法中将事件传递给手势检测对象，否则手势监听对象中的回调函数是不会被调用的
		mGestureDetector.onTouchEvent(event);

		//获取相对屏幕的坐标，即以屏幕左上角为原点		 
		x = event.getRawX();   
		y = event.getRawY() - 25;   //25是系统状态栏的高度
		Log.i("currP", "currX = "+ x +" ,currY = " + y);
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
     		//获取相对View的坐标，即以此View左上角为原点
        	mTouchStartX =  event.getX();
            mTouchStartY =  event.getY();
            updateViewPosition();
            Log.i("startP", "startX = " + mTouchStartX + " ,startY = " + mTouchStartY);
            break;
		case MotionEvent.ACTION_MOVE:	            
            updateViewPosition();
            break;
     	case MotionEvent.ACTION_UP:
        	updateViewPosition();
        	mTouchStartX = mTouchStartY = 0;
        	break;
		}
		return true;
	}
	 
	private void updateViewPosition(){
		//更新浮动窗口位置参数
		mWindowParams.x=(int)(x - mTouchStartX);
		mWindowParams.y=(int)(y - mTouchStartY);
		if(mIsShowIcon) {
	        mWindowParams.width = Util.convertDpToPixel(64);
	        mWindowParams.height = Util.convertDpToPixel(64);
			mWindowManager.updateViewLayout(mIconView, mWindowParams);
		}
		if(mIsShowVideo) {
	        mWindowParams.width = mVideoWidth;
	        mWindowParams.height = mVideoHeight;
		    mWindowManager.updateViewLayout(mVideoView, mWindowParams);
		}
	}
	
	private void showIcon() {
		Log.i(TAG, "showIcon called.");
		if(!mIsShowIcon) {
			mWindowManager.addView(mIconView, mWindowParams);
			mIsShowIcon = true;
		}
	}

	private void closeIcon() {
		Log.i(TAG, "closeIcon called.");
		if(mIsShowIcon) {
			mWindowManager.removeViewImmediate(mIconView);
			mIsShowIcon = false;
		}
	}
	
	private void showVideo() {
		Log.i(TAG, "showVideo called.");
		if(!mIsShowVideo) {
			mWindowManager.addView(mVideoView, mWindowParams);
			mIsShowVideo = true;
		}
	}

	private void closeVideo() {
		Log.i(TAG, "closeVideo called.");
		if(mIsShowVideo) {
			mWindowManager.removeViewImmediate(mVideoView);
			mIsShowVideo = false;
		}
	}

	/*========================================================================
	 *                            View.OnTouchListener
	 =========================================================================*/
	private View.OnTouchListener mViewOnTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
		{
			onWindowTouchEvent(paramMotionEvent);
			return false;
		}
	};

	/*========================================================================
	 *                            SurfaceHolder.Callback
	 =========================================================================*/
	private SurfaceHolder.Callback mSurfaceHolderCallback = new SurfaceHolder.Callback() {
		@Override
    	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    	{
			mVideoWidth = width;
			mVideoHeight = height;
    		Log.d(TAG, "surfaceChanged Called: width = " + width + " ,height = " + height);
    	}

    	@Override
    	public void surfaceCreated(SurfaceHolder holder)
    	{
    		MediaCodecManager codecManager = MediaCodecManager.getInstance();
    		codecManager.setSurface(holder.getSurface());
    		codecManager.Start();
    		
    		Log.d(TAG, "surfaceCreated Called");
    	}

    	@Override
    	public void surfaceDestroyed(SurfaceHolder holder)
    	{
    		MediaCodecManager codecManager = MediaCodecManager.getInstance();
    		codecManager.Stop();
    		codecManager.setSurface(null);
    		
    		Log.d(TAG, "surfaceDestroyed Called");
    	}
	};

	/*========================================================================
	 *                            GestureDetector
	 =========================================================================*/
	private OnGestureListener mOnGestureListener = new OnGestureListener() {
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			Log.i(TAG, "onSingleTapUp: " + e.toString());
			if(!mIsShowVideo) {
				showVideo();
			} else {
				closeVideo();
			}

			return false;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			Log.i(TAG, "onScroll: " + e1.toString() + ", " + e2.toString());
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			Log.i(TAG, "onLongPress: " + e.toString());
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			Log.i(TAG, "onFling: " + e1.toString() + ", " + e2.toString());
			return false;
		}

		@Override
		public boolean onDown(MotionEvent e) {
			Log.i(TAG, "onDown: " + e.toString());
			return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {
			Log.i(TAG, "onShowPress: " + e.toString());
		}
	};
}
