package org.camera.codec;

import org.camera.config.ConfigEntity;

import android.util.Log;
import android.view.Surface;

public class MediaCodecManager {
	private static final String TAG = "MediaCodecManager";
	private static MediaCodecManager mMediaCodecManager = null;
	public static final int MEDIACODEC_ID_HW = 0;
	public static final int MEDIACODEC_ID_SW = 1;
	private int mSelectedCodec = MEDIACODEC_ID_HW;
	private boolean mIsRunning = false;
	
	private Codec mHWCodec = MediaHWCodec.getInstance(this);			// HWCodec只能实例化一个，多个实例无法同时工作.
	private Codec mSWCodec = MediaSWCodec.getInstance(this);
	
	public interface CodecInterface {
		public void Init();
		public void InitEncoder();
		public void InitDecoder();
		public	int Encode(byte[] input, byte[] output);
		public void Decode(byte[] input, int inputSize);
		public void CloseEncoder();
		public void CloseDecoder();
	};
	
	// base class of MediaCodec
	public class Codec implements CodecInterface {
		protected int mWidth;
		protected int mHeight;
		protected int mFrameRate;										// fps
		protected int mIFrameInterval;									// seconds between I-frames
		protected int mBitRate;
		protected Surface mSurface = null;
		@Override
		public void Init() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void InitEncoder() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void InitDecoder() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public	int Encode(byte[] input, byte[] output) {
			// TODO Auto-generated method stub
			return 0;
		}
		@Override
		public void Decode(byte[] input, int inputSize) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void CloseEncoder() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void CloseDecoder() {
			// TODO Auto-generated method stub
			
		}
	};
	
	public class Size {
		public int mWidth;
		public int mHeight;
		
		public Size(int width, int height) {
			mWidth = width;
			mHeight = height;
		}
	};

	// MediaCodecManager����ģʽ
	public static synchronized MediaCodecManager getInstance() {
		if (mMediaCodecManager == null) {
			mMediaCodecManager = new MediaCodecManager();
			mMediaCodecManager.Init();
		}
		return mMediaCodecManager;
	}

	public void setSurface(Surface surface) {
		mHWCodec.mSurface = surface;
		mSWCodec.mSurface = surface;
	}
	
	public void selectCodec(int codecId) {
		mSelectedCodec = codecId;
	}
	
	public synchronized void Init() {
		// load video config
		mHWCodec.mWidth				= mSWCodec.mWidth			= ConfigEntity.RESOLUTION_WIDTH;
		mHWCodec.mHeight 			= mSWCodec.mHeight			= ConfigEntity.RESOLUTION_HEIGHT;
		mHWCodec.mFrameRate			= mSWCodec.mFrameRate		= ConfigEntity.VIDEO_FPS;
		mHWCodec.mBitRate 			= mSWCodec.mBitRate			= ConfigEntity.VIDEO_BITRATE;
		mHWCodec.mIFrameInterval 	= mSWCodec.mIFrameInterval	= ConfigEntity.CODEC_IFRAME_INTERVAL;
		
		// init HW/SW codec
		mHWCodec.Init();
		mSWCodec.Init();
	}
	
	public synchronized void Start() {
		Codec codec = getCodec();
		
		if(codec == null) {
			Log.e(TAG, "codec was not created!");
			return;
		}
		
		codec.InitEncoder();
		codec.InitDecoder();
		mIsRunning = true;
	}
	
	public synchronized int Encode(byte[] input, byte[] output) {
		Codec codec = getCodec();
		
		if(codec == null) {
			Log.e(TAG, "codec was not created!");
			return 0;
		}
		if(!mIsRunning) {
			Log.e(TAG, "encoder was not running!");
			return 0;
		}
		return codec.Encode(input, output);
	}
	
	public synchronized void Decode(byte[] input, int inputSize) {
		Codec codec = getCodec();
		
		if(codec == null) {
			Log.e(TAG, "codec was not created!");
			return;
		}
		if(!mIsRunning) {
			Log.e(TAG, "decoder was not running!");
			return;
		}
		codec.Decode(input, inputSize);
	}

	public synchronized void Stop() {
		Codec codec = getCodec();
		
		if(codec == null) {
			Log.e(TAG, "codec was not created!");
			return;
		}
		codec.CloseEncoder();
		codec.CloseDecoder();
		mIsRunning = false;
	}

	private Codec getCodec() {
		Codec codec = null;
		if(mSelectedCodec == MEDIACODEC_ID_HW) {
			codec = mHWCodec;
		} else {
			codec = mSWCodec;
		}
		
		return codec;
	}
}
