package org.camera.codec;
 
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaCodec.BufferInfo;
import android.util.Log;
 
public class MediaHWCodec extends MediaCodecManager.Codec {
	private static final String TAG = "MediaHWCodec";
	private static final boolean VERBOSE = true;					// lots of logging
	// save dump file for debug
	private static final boolean DEBUG_SAVE_DUMP_FILE = false;		// save video file for DEBUG
	private static final String DEBUG_FILE_NAME_BASE = "sdcard/Movies/H264";
	FileOutputStream mEncodeOutputStream = null;
	FileOutputStream mDecodeInputStream = null;
	// parameters for the HWCodec
	private static final String MIME_TYPE = "video/avc";			// H.264 Advanced Video
	private static final int TIMEOUT_USEC = 10000;					// bit rate CameraWrapper.
	private static MediaHWCodec mMediaHWCodec;
	private MediaCodec mEncoder = null;
	private MediaCodec mDecoder = null;
	private byte[] mHeadinfo = null; 
	private byte[] mFrameData = null;
	private BufferInfo mBufferInfo;
	private MediaFormat mMediaFormat;
	private int mColorFormat;
	private int mVideoBufferSize;
	private long mStartTime = 0;

	// MediaHWCodec为单例模式
	public static synchronized MediaHWCodec getInstance(MediaCodecManager codecManager) {
		if (mMediaHWCodec == null) {
			mMediaHWCodec = new MediaHWCodec(codecManager);
		}
		return mMediaHWCodec;
	}

	public MediaHWCodec(MediaCodecManager codecManager) {
		codecManager.super();
	}

	public void Init() {
		mVideoBufferSize 	= mWidth * mHeight * 3 / 2;		// 榛樿YUV420鏍煎紡
		mFrameData			= new byte[mVideoBufferSize];
		mBufferInfo = new MediaCodec.BufferInfo();

		// get codec info
		MediaCodecInfo codecInfo = selectCodec(MIME_TYPE);
		if (codecInfo == null) {
			Log.e(TAG, "Unable to find an appropriate codec for " + MIME_TYPE);
			return;
		}
		if (VERBOSE) Log.d(TAG, "found codec: " + codecInfo.getName());
		mColorFormat = selectColorFormat(codecInfo, MIME_TYPE);
		if (VERBOSE) Log.d(TAG, "found colorFormat: " + mColorFormat);

		// get media format
		mMediaFormat = MediaFormat.createVideoFormat(MIME_TYPE, this.mWidth, this.mHeight);
		mMediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, mBitRate);
		mMediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, mFrameRate);
		mMediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, mColorFormat);
		mMediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, mIFrameInterval);
		if (VERBOSE) Log.d(TAG, "format: " + mMediaFormat);

		mStartTime = System.nanoTime();

		// init dump file
		if(DEBUG_SAVE_DUMP_FILE) {
			InitDumpFile();
		}
	}
 
	public synchronized void InitEncoder() {
		Log.e(TAG, "InitEncoder");
		if(mEncoder != null) {
			Log.d(TAG, "stop and release previous encoder!");
			try {
				mEncoder.stop();
				mEncoder.release();
				mEncoder = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mEncoder = MediaCodec.createEncoderByType(MIME_TYPE);
		mEncoder.configure(mMediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
		mEncoder.start();
		Log.d(TAG, "Encoder inited!");
	}
   
	public synchronized void InitDecoder() {
		Log.e(TAG, "InitDecoder");
		if(mDecoder != null) {
			Log.d(TAG, "stop and release previous decoder!");
			try {
				mDecoder.stop();
				mDecoder.release();
				mDecoder = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mDecoder = MediaCodec.createDecoderByType(MIME_TYPE);
		mDecoder.configure(mMediaFormat, mSurface, null, 0);
		mDecoder.start();
	}
   
	public synchronized int Encode(byte[] input, byte[] output) {
		int pos = 0;
		NV21toI420SemiPlanar(input, mFrameData, this.mWidth, this.mHeight);

		ByteBuffer[] inputBuffers = mEncoder.getInputBuffers();
		ByteBuffer[] outputBuffers = mEncoder.getOutputBuffers();
		int inputBufferIndex = mEncoder.dequeueInputBuffer(TIMEOUT_USEC);
		if (VERBOSE) Log.i(TAG, "inputBufferIndex-->" + inputBufferIndex);
		if (inputBufferIndex >= 0) {
			long endTime = System.nanoTime();
			long ptsUsec = (endTime - mStartTime) / 1000;
			Log.i(TAG, "resentationTime: " + ptsUsec);
			ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
			inputBuffer.clear();
			inputBuffer.put(mFrameData);
			mEncoder.queueInputBuffer(inputBufferIndex, 0, mFrameData.length, System.nanoTime() / 1000, 0);
		} else {
			// either all in use, or we timed out during initial setup
			if (VERBOSE) Log.d(TAG, "input buffer not available");
		}

		int outputBufferIndex = mEncoder.dequeueOutputBuffer(mBufferInfo, TIMEOUT_USEC);
		Log.i(TAG, "outputBufferIndex-->" + outputBufferIndex);
		do {
			if (outputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
				// no output available yet
				if (VERBOSE) Log.d(TAG, "no output from encoder available");
			} else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
				// not expected for an encoder
				outputBuffers = mEncoder.getOutputBuffers();
				if (VERBOSE) Log.d(TAG, "encoder output buffers changed");
			} else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
				// not expected for an encoder
				MediaFormat newFormat = mEncoder.getOutputFormat();
				Log.d(TAG, "encoder output format changed: " + newFormat);
			} else if (outputBufferIndex < 0) {
				Log.w(TAG, "unexpected result from encoder.dequeueOutputBuffer: " + outputBufferIndex);
				// let's ignore it
			} else {
				if (VERBOSE) Log.d(TAG, "perform encoding");
				ByteBuffer outputBuffer = outputBuffers[outputBufferIndex];
				if (outputBuffer == null) {
					throw new RuntimeException("encoderOutputBuffer " + outputBufferIndex + " was null");
				}

				if (mBufferInfo.size != 0) {
					// 根据BufferInfo调整outputBuffer数据开始点和结束点，用于后面get。
					outputBuffer.position(mBufferInfo.offset);
					outputBuffer.limit(mBufferInfo.offset + mBufferInfo.size);

					// 从outputBuffer中取出数据放入outData
					byte[] outData = new byte[mBufferInfo.size];  
					outputBuffer.get(outData);

					if(mHeadinfo != null) {
						// 将outData写入output。因为一次编码未必能完成inputbuffer的所有encode工作，因此这里给一个while循环。
						System.arraycopy(outData, 0, output, pos, outData.length);
						pos += outData.length;
					} else {
						// 保存pps sps, 只有开始时 第一个帧里有， 保存起来后面用.
						ByteBuffer spsPpsBuffer = ByteBuffer.wrap(outData);    
						if (spsPpsBuffer.getInt() == 0x00000001) {
							mHeadinfo = new byte[outData.length];
							System.arraycopy(outData, 0, mHeadinfo, 0, outData.length);  
						}
					}
				}

				mEncoder.releaseOutputBuffer(outputBufferIndex, false);
			}
			outputBufferIndex = mEncoder.dequeueOutputBuffer(mBufferInfo, TIMEOUT_USEC);
		} while (outputBufferIndex >= 0);

		// 有些编码器生成关键帧(I帧)时只有 00 00 00 01 65, 没有pps sps, 要加上  
        if(output[4] == 0x65) {
			System.arraycopy(output, 0, mFrameData, 0, pos);
			System.arraycopy(mHeadinfo, 0,  output, 0, mHeadinfo.length);
			System.arraycopy(mFrameData, 0,  output, mHeadinfo.length, pos);
			pos += mHeadinfo.length;
        }
        
        // 保存encode output dump file
        if(DEBUG_SAVE_DUMP_FILE) {
        	WriteDumpFile(output, pos, true);
        }
        
        return pos;
	}
 
	public synchronized void Decode(byte[] input, int inputSize) {
		// save decode input dump file
		if(DEBUG_SAVE_DUMP_FILE) {
			WriteDumpFile(input, inputSize, false);
		}

		ByteBuffer[] inputBuffers = mDecoder.getInputBuffers();
		int inputBufferIndex = mDecoder.dequeueInputBuffer(-1);
		if (inputBufferIndex >= 0) {
			ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
			inputBuffer.clear();
			inputBuffer.put(input, 0, inputSize);
			mDecoder.queueInputBuffer(inputBufferIndex, 0, inputSize, System.nanoTime() / 1000, 0);
		}

		MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
		int outputBufferIndex = mDecoder.dequeueOutputBuffer(bufferInfo,0);
		while (outputBufferIndex >= 0) {
			mDecoder.releaseOutputBuffer(outputBufferIndex, true);
			outputBufferIndex = mDecoder.dequeueOutputBuffer(bufferInfo, 0);
		}
	}

	@SuppressLint("NewApi")
	public synchronized void CloseEncoder() {    
		Log.i(TAG, "CloseEncoder()");

		if(DEBUG_SAVE_DUMP_FILE) {
			CloseDumpFile(true);
		}

		try {
			mEncoder.stop();
			mEncoder.release();
			mEncoder = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressLint("NewApi")
	public synchronized void CloseDecoder() {
		Log.i(TAG, "CloseDecoder()");

		if(DEBUG_SAVE_DUMP_FILE) {
			CloseDumpFile(false);
		}

		try {
			mDecoder.stop();
			mDecoder.release();
			mDecoder = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** dump file for debug */
	private void InitDumpFile() {
		String encodeFileName = DEBUG_FILE_NAME_BASE + "Enc" + mWidth + "x" + mHeight + ".mp4";
		String decodeFileName = DEBUG_FILE_NAME_BASE + "Dec" + mWidth + "x" + mHeight + ".mp4";
		try {
			mEncodeOutputStream = new FileOutputStream(encodeFileName);
			mDecodeInputStream = new FileOutputStream(decodeFileName);
			Log.d(TAG, "dump file will be saved as " + encodeFileName + " and " + decodeFileName);
		} catch (IOException ioe) {
			Log.w(TAG, "Unable to create debug output file!");
			throw new RuntimeException(ioe);
		}
	}

	private void WriteDumpFile(byte[] data, int size, boolean isEncode) {
		if(data == null) {
			Log.w(TAG, "dump data is null!");
			return;
		}

		if(isEncode) {
			if(mEncodeOutputStream != null) {
				try {
					mEncodeOutputStream.write(data, 0, size);
				} catch (IOException ioe) {
					Log.w(TAG, "failed writing encode dump to file!");
					throw new RuntimeException(ioe);
				}
			}
		} else {
			if(mDecodeInputStream != null) {
				try {
					mDecodeInputStream.write(data, 0, size);
				} catch (IOException ioe) {
					Log.w(TAG, "failed writing decode dump to file!");
					throw new RuntimeException(ioe);
				}
			}
		}
	}

	private void CloseDumpFile(boolean isEncode) {
		if(isEncode) {
			if (mEncodeOutputStream != null) {
				try {
					mEncodeOutputStream.close();
				} catch (IOException ioe) {
					Log.w(TAG, "failed closing encode debug file!");
					throw new RuntimeException(ioe);
				}
			}
		} else {
			if (mDecodeInputStream != null) {
				try {
					mDecodeInputStream.close();
				} catch (IOException ioe) {
					Log.w(TAG, "failed closing decode debug file!");
					throw new RuntimeException(ioe);
				}
			}
		}
	}

	/**
	* NV21 is a 4:2:0 YCbCr, For 1 NV21 pixel: YYYYYYYY VUVU I420YUVSemiPlanar
	* is a 4:2:0 YUV, For a single I420 pixel: YYYYYYYY UVUV Apply NV21 to
	* I420YUVSemiPlanar(NV12) Refer to https://wiki.videolan.org/YUV/
	*/
	private void NV21toI420SemiPlanar(byte[] nv21bytes, byte[] i420bytes, int width, int height) {
		System.arraycopy(nv21bytes, 0, i420bytes, 0, width * height);
		for (int i = width * height; i < nv21bytes.length; i += 2) {
			i420bytes[i] = nv21bytes[i + 1];
			i420bytes[i + 1] = nv21bytes[i];
		}
	}

	/**
	* Returns a color format that is supported by the codec and by this test
	* code. If no match is found, this throws a test failure -- the set of
	* formats known to the test should be expanded for new platforms.
	*/
	private static int selectColorFormat(MediaCodecInfo codecInfo, String mimeType) {
		MediaCodecInfo.CodecCapabilities capabilities = codecInfo.getCapabilitiesForType(mimeType);
		for (int i = 0; i < capabilities.colorFormats.length; i++) {
			int colorFormat = capabilities.colorFormats[i];
			if (isRecognizedFormat(colorFormat)) {
				return colorFormat;
			}
		}
		Log.e(TAG, "couldn't find a good color format for " + codecInfo.getName() + " / " + mimeType);
		return 0; // not reached
	}
 
	/**
	* Returns true if this is a color format that this test code understands
	* (i.e. we know how to read and generate frames in this format).
	*/
	private static boolean isRecognizedFormat(int colorFormat) {
		switch (colorFormat) {
		// these are the formats we know how to handle for this test
		case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
		case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
		case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
		case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
		case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
			return true;
		default:
			return false;
		}
	}

	/**
	* Returns the first codec capable of encoding the specified MIME type, or
	* null if no match was found.
	*/
	private static MediaCodecInfo selectCodec(String mimeType) {
		int numCodecs = MediaCodecList.getCodecCount();
		for (int i = 0; i < numCodecs; i++) {
			MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
			if (!codecInfo.isEncoder()) {
				continue;
			}
			String[] types = codecInfo.getSupportedTypes();
			for (int j = 0; j < types.length; j++) {
				if (types[j].equalsIgnoreCase(mimeType)) {
					return codecInfo;
				}
			}
		}
		return null;
	}

}