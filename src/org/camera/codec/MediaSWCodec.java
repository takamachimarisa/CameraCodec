package org.camera.codec;

import android.view.Surface;

public class MediaSWCodec extends MediaCodecManager.Codec {
	private static final String TAG = "MediaSWCodec";
	private static MediaHWCodec mMediaSWCodec;
	
	// SWCodec type
	private static final int MEDIASWCODEC_H263 = 0;
	private static final int MEDIASWCODEC_H264 = 1;
	private static final int MEDIASWCODEC_H265 = 2;
	// parameters for the SWCodec
	private static final int CODEC_TYPE = MEDIASWCODEC_H264;
	
	public static synchronized MediaHWCodec getInstance(MediaCodecManager codecManager) {
		if (mMediaSWCodec == null) {
			mMediaSWCodec = new MediaHWCodec(codecManager);
		}
		return mMediaSWCodec;
	}
	
	public MediaSWCodec(MediaCodecManager codecManager) {
		codecManager.super();
	}
	
	public void Init() {
		native_setup(CODEC_TYPE, mWidth, mHeight, mSurface);
	}
	
	/* jni functions */
    private native final void native_setup(int codecType, int width, int height, Surface surface);
}
